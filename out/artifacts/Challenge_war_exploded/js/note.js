let note = {};
let selectedDate;

function deleteRow(element) {
    let id = element.parent().attr("id");
    let urlParam = 'DeleteNote?TYPE_REQ=DEL_P&ROW_TO='+id;
    $.post(urlParam).done(function () {
        element.parent().parent().remove();
        console.log("Registro deletado com sucesso");
    })
}


function onSelectDate(date){
    selectedDate = date;
    let urlParam = 'NoteServ?OBJ_DATE='+date+'&TYPE_REQ=getlist';
    $.get(urlParam).done(function (data) {
        $("#note-table").find("tr:gt(0)").remove();
        for(var i = 0; i <= data.length-1; i++){
            let output = '<tr>' +
                '<td class="start">'+data[i].id+'</td>'+
                '<td class="middle">'+data[i].desc+'</td>'+
                '<td id="'+data[i].id+'" class="end"><img src="img/remove.png" class="deleteImg" onclick="deleteRow($(this))"></td>'+
                '</tr>';
            $('#note-table tr:first').after(output);
        }
    });
}



function getList(date, onSuccess, onError) {
    let type = 'GETLIST';
    let url ="http://localhost:9191/ws-note/NoteServ";
    executeAjax(url,{
        'OBJ_DATE': date,
        'TYPE_REQ': type
    }, onSuccess, onError)
}

/*executeAjax(http('GetSingle'),
    {
        'DAO_CLASS': btoa(dao),
        'SEARCHING': btoa(searching)
    }, onSucess, onError);*/

function executeAjax(url,dataPost, onSucess, onError) {
    $.ajax({
        url: url,
        data: dataPost,
        type: 'GET',
        async: false,
        timeout: 120000,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {

            onSucess(data);
        },
        error: function (data) {
            if (onError) {
                onError(data);
            }
        }
    });
}


function createNote(title, desc) {
    note.id = 0;
    note.desc = desc;
    note.title = title;
    if(selectedDate){
        note.noteDate = selectedDate;
    } else {
        note.noteDate = new Date();
    }
}

