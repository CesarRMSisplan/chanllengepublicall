<%--
  Created by IntelliJ IDEA.
  User: Cesar
  Date: 17/03/20
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <link rel="stylesheet" href="css/calendar.css"/>
  </head>

  <script src="js_external/jquery-1.12.3.min.js"></script>
  <script src="js_external/jquery-migrate-1.4.1.min.js"></script>
  <script src="js_external/jquery-ui-1.11.4.js"></script>
  <script src="js/note.js"></script>


  <script>
      function insertRow(note) {
          let urlParam = 'InsertNote?TYPE_REQ=INS_P&OBJ_TI='+note.title+"&OBJ_DES="+note.desc+"&OBJ_DAT="+note.noteDate;
          $.post(urlParam).done(function () {
              console.log("Registro inserido com sucesso");
          })
      }


          $(function () {
              $('#bt-clear').click(function () {
                  $('#title-in').val("");
                  $('#desc-in').val("");
                  note = {};
                  window.location.reload();
              });

              $('#bt-save').click(function () {
                  let title = $('#title-in').val();
                  let desc = $('#desc-in').val();
                  if(title && desc){
                      createNote(title, desc);
                      insertRow(note);
                  }
                 $('#bt-clear').trigger('click');
              });

                $('#divPicker').datepicker({
                      numberOfMonths: 1,
                      setDate: new Date(),
                      dateFormat: 'dd/mm/yy',
                      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                      dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                      monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                      onSelect: function (dateText, inst) {
                          //onDate(dateText, $(this).datepicker('getDate'));
                            onSelectDate(dateText);
                      }
                });
          });

</script>

<body>


      <div id="container-left" class="content-left">
          <div id="divPicker"></div>
      </div>

      <div style="display: inline-block; float: right; width: 37%;">
        <div id="container-right" class="content-right">
            <div id="body-node" class="content-header">
                <input id="title-in" type="text" placeholder="Título da nota" rows="1" maxlength="100" class="input-title def-input" />
                <input id="desc-in" type="text" placeholder="Texto da nota" rows="1" maxlength="1000" class="input-text def-input" />


                <div id="container-buttons" class="content-button">
                    <button id="bt-clear" type="button" class="bt-clear bt-def">Limpar</button>
                    <button id="bt-save" type="button" class="bt-save bt-def">Salvar</button>
                </div>
            </div>

            <div id="list-note" class="content-list">
                <table id="note-table" class="note-table">
                    <tr class="line">
                        <td class="start header">Id</td>
                        <td class="middle header">Nota</td>
                        <td class="end"></td>
                    </tr>
                </table>
            </div>
        </div>
      </div>


  </body>
</html>
