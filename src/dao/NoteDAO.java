package dao;

import bean.Note;
import cnt.NoteCnt;
import connection.DataBaseConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class NoteDAO {

    public void insertNote(Note note, String strDate) throws SQLException {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            String query = "INSERT INTO " + NoteCnt.TABLE + "(ID,TITLE,DESCRIPTION, DATENOTE)VALUES('" + note.getId() + "','" + note.getTitle() + "','" + note.getDesc() + "', cast('" + strDate + "' as DATE))";

            conn = DataBaseConnection.get();
            st = conn.createStatement();
            Boolean was = st.execute(query);

            if (was) {
                System.out.println("Nota inserida com sucesso!");
            } else {
                System.out.println("Falha ao inserir!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Note> getList(String date) throws Exception {
        ArrayList<Note> arrayList = new ArrayList<>();
        String[] arrStr = date.split("/");
        String dt_final = arrStr[2] + "-" + arrStr[1] + "-" + arrStr[0];

        String query = "SELECT * FROM " + NoteCnt.TABLE + " WHERE " + NoteCnt.DATE + " = cast('" + dt_final + "' as DATE)";

        Connection conn = DataBaseConnection.get();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(query);


        while (rs.next()) {
            Note note = new Note(rs.getInt(NoteCnt.ID), rs.getString(NoteCnt.TITLE), rs.getString(NoteCnt.DESCRIPTION), rs.getDate(NoteCnt.DATE));
            if (note != null) {
                arrayList.add(note);
            }
        }


        return arrayList;
    }


    public void delete(int id) throws Exception {
        String q = "DELETE FROM " + NoteCnt.TABLE + " WHERE ID=" + id;

        Connection conn = DataBaseConnection.get();
        Statement st = conn.createStatement();
        Boolean rs = st.execute(q);


        while (rs) {
            System.out.println("Registro de id=" + id + " deletado com sucesso");
        }
    }


}
