package bean;

import java.util.Date;

public class Note {

    private int id;
    private String title;
    private String desc;
    private Date noteDate;

    public Date getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(Date noteDate) {
        this.noteDate = noteDate;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDesc(){
        return this.desc;
    }

    public void setDesc(String desc){
        this.desc = desc;
    }

    public Note(int id, String title, String desc, Date noteDate){
        this.id = id;
        this.desc = desc;
        this.title = title;
        this.noteDate = noteDate;
    }

}
