package servlets;

import bean.Note;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import dao.NoteDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

public class NoteServ extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NoteDAO noteDAO = new NoteDAO();

        String paramDate = req.getParameter("OBJ_DATE");
        String typeReq = req.getParameter("TYPE_REQ");

        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();
        PrintWriter out = null;

        if (typeReq != null && typeReq.equalsIgnoreCase("getlist")) {
            try {
                List<Note> noteList = noteDAO.getList(paramDate);
                String gsonObj = gson.toJson(noteList, new TypeToken<List<Note>>() {
                }.getType());
                json(resp, gsonObj);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void write(HttpServletResponse response, String r) throws IOException {
        PrintWriter out = response.getWriter();
        out.write(r);
        out.flush();
        out.close();
    }

    public static void json(HttpServletResponse response, String r) throws IOException {
        response.setHeader("Content-Type", "application/json");
        write(response, r);
    }


}
