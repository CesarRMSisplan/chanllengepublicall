package servlets;

import bean.Note;
import dao.NoteDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class InsertNote extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String typeReq = req.getParameter("TYPE_REQ");
        NoteDAO noteDAO = new NoteDAO();

        if (typeReq != null && typeReq.equalsIgnoreCase("INS_P")) {
            String title = req.getParameter("OBJ_TI");
            String desc = req.getParameter("OBJ_DES");
            SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");

            try {
                String[] arr_str = req.getParameter("OBJ_DAT").split("/");
                String dt_str_f = arr_str[2] + "-" + arr_str[1] + "-" + arr_str[0];

                Note note = new Note(0, title, desc, null);
                if (note != null)
                    noteDAO.insertNote(note, dt_str_f);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }
}
