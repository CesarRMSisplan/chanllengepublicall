package servlets;

import dao.NoteDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteNote extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String typeReq = req.getParameter("TYPE_REQ");

        NoteDAO noteDAO = new NoteDAO();

         if (typeReq != null && typeReq.equalsIgnoreCase("DEL_P")) {
            try {
                String rowToDelete = req.getParameter("ROW_TO");
                noteDAO.delete(Integer.parseInt(rowToDelete));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
