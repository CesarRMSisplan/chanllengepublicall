package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Locale;

public class DataBaseConnection {

    private static int times = 0;
    private static Connection conn;
    private static String CLASSNAME = "org.firebirdsql.jdbc.FBDriver";
    private static String URL = "jdbc:firebirdsql:127.0.0.1/3050:C:/Users/Cesar Roberto/Documents/Challenge/src/base/CALENDAR.FDB?sql_dialect=3&charSet=ISO-8859-1";
    private static String USER = "SYSDBA";
    private static String PASS = "masterkey";


    public static Connection get() throws Exception {
        if (conn == null) {
            try {
                Locale.setDefault(Locale.ENGLISH);
                Class.forName(CLASSNAME);
                conn = DriverManager.getConnection(URL, USER, PASS);

                return conn;
            } catch (ClassNotFoundException e) {
                throw new ClassNotFoundException(String.format("Driver de banco não foi encontrado.", CLASSNAME), e);
            }
        } else {
            times++;
            return conn;
        }
    }

}
